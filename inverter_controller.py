#!/usr/bin/env python
'''
Controls inverters given a schedule and grid values
Tested with Fronius Gen24 and Sungrow SH10RT, 2023 Per Carlen
'''

import time
import re
import sys
import json

from pymodbus.client import ModbusTcpClient as ModbusClient
import pytz
import struct
import time
import requests
import os
import yaml
from datetime import datetime
import paho.mqtt.client as paho
from typing import List,Dict
import binascii
import asyncio
import syslog
import copy
import curses
from curses import wrapper

class InverterController:
  def __init__(self):
    self.config = self.read_config_file('config.yaml')

  @staticmethod
  def read_config_file(filename: str) -> Dict:
    config = ""
    filename = "inverter_controller.yaml"
    path = "/etc/inverter_controller/"
    if os.path.isfile(path+filename):
      filename = path + filename
    print("Reading from:" + filename + "\n")
    try:
      with open(filename,'r') as file:
        config = yaml.safe_load(file)
    except:
      print("ERROR: No config file - ",filename)
      exit(1)
    return config


# --------------------------------------------------------------------------- #
# configure logging
# --------------------------------------------------------------------------- #
import logging


def get_value_from_32bits(r1,r2):

  v = r1 + (r2 & 32767) * 65536
  if r2 > 32768:
    v = - ((65535 - r1) + (65535 - r2) * 65536)
  return v


def handle_vpp_control_message(vpp_control):
  global external_vpp_stopped
  global external_vpp_passthrough
  global inverter_data
  global client
  global config

  now = datetime.now()

  if config['debug']: print_win("vpp","Got VPP command" + str(vpp_control) + ", VPP passthrough:" + str(external_vpp_passthrough) + "\n")

  if 'cmd' in vpp_control and 'power' in vpp_control:
    mode = ""
    if vpp_control['cmd'] == "stop":
      external_vpp_stopped = True
    power = int(vpp_control['power'])
    if external_vpp_passthrough:  # Passes all VPP commands through
      external_vpp_stopped = False
      if vpp_control['cmd'] == 'stop':
        mode = "stopped"
      if vpp_control['cmd'] == 'discharge':
        mode = "discharging"
        power = -power
      if vpp_control['cmd'] == 'charge':
        mode = "charging"
    else: # only do things when stopped
      if vpp_control['cmd'] != "stop":
        external_vpp_stopped = False
        if vpp_control['cmd'] == 'discharge':
          mode = "discharging"
          power = -power
        if vpp_control['cmd'] == 'charge':
          mode = "charging"
      else:
        print_win("vpp",str(now) + " - Got VPP command" + str(vpp_control) + ", VPP passthrough:" + str(external_vpp_passthrough) + "\n")
    if mode != "":
      i = 0
      while i < len(inverter_data):
        if inverter_data[i]['connected']:
          if inverter_data[i]['operating_mode'] != "virtual_power_plant":
            print_win("vpp","Got VPP command to charge/discharge, but not in that mode, ignoring!\n")              
            return
          else:
            print_win("vpp"," VPP Setting power to: " + str(power) + "W for " + inverter_data[i]['name'] + " - " + mode + ", VPP passthrough:" + str(external_vpp_passthrough) + "\n")
            set_power(client[i],inverter_data[i],abs(power),mode,"")
        i += 1      


def mqtt_on_message(mqtt_client,userdata, message,tmp=None):
  global config
  global last_updated
  global p_grid
  global e_consumed
  global e_produced
  global inverter_monitor_data
  global inverter_data
  global adjust_now
  global vpp_control


  if message.topic == "inverter/monitor":
    #if config['debug']: print_win("debug","Got mqtt update from inverter" + str(message.payload))
    json_data = json.loads(message.payload)
    if type(json_data) is dict or type(json_data) is list:        
      i = -1
      for index,i_data in enumerate(inverter_data):
        if i_data['name'] == json_data['name']:
          i = index
      if i > -1:
        inverter_data[i].update(json_data)
        #if config['debug']: print_win("debug","new inverter data:" + str(inverter_data[i]))

    if config['follow_inverter'][0]['name'] == json_data['name'] and config['follow_inverter'][0]['enabled'] == True:
      print_win("log","----\n")
      print_win("log","FOLLOW ME:" + str(json_data) + "\n")
      print_win("log","----\n")
      inverter_monitor_data.update(json_data)
      adjust_now = True

  if message.topic == "meter/data":
    if config['debug']: print_win("debug","Got mqtt update from meter\n")
    json_data = json.loads(message.payload)
    if type(json_data) is dict or type(json_data) is list:
      last_updated = json_data['last_updated']
      p_grid = json_data['pt']
      e_consumed = json_data['e_consumed']
      e_produced = json_data['e_produced']

  if message.topic == "vpp/control":
    if config['debug']: print_win("debug","Got mqtt update from vpp" + str(message.payload) + "\n")
    json_data = json.loads(message.payload)
    if type(json_data) is dict or type(json_data) is list:
      if config['inverters'][0]['name'] in json_data: # For me?
        #vpp_control = json_data[config['inverters'][0]['name']]
        vpp_control = {**vpp_control, **json_data[config['inverters'][0]['name']]}
        if config['debug']: print_win("debug","VPP command to me personally:" + str(vpp_control))
      else:
        if config['debug']: print_win("debug","VPP command to all(old school):" + str(json_data))
        vpp_control = {**vpp_control, **json_data}
      handle_vpp_control_message(vpp_control)


def mqtt_on_subscribe(client, userdata, mid, granted_qos):
    #print_win("log"," Received message " + str(client)
    #    + "' with QoS " + str(granted_qos))
    pass


def mqtt_on_connect(client, userdata, flags, rc):
  global config

  if rc != 0:
    print_win("log","MQTT connection problem\n")
    client.connected_flag=False
  else:
    print_win("log","MQTT client connected:" + str(client) + "\n")
    client.connected_flag=True
    client.subscribe("meter/data",2)
    client.subscribe("inverter/monitor",2)
    if config['external_vpp_overrides']:
      client.subscribe("vpp/control",2)


def run_mqtt_client(config):
  global mqtt_client_pub
  global mqtt_client_sub
  global mqtt_client_smarthome
  global smarthome

  print_win("log","Starting mqtt handler\n")
  if config:
    mqtt_broker = config['mqtt_broker']
    mqtt_port = config['mqtt_port']
    mqtt_username = config['mqtt_username']
    mqtt_password = config['mqtt_password']
  else:
    exit(1)

  random_string = str(binascii.b2a_hex(os.urandom(8)))

  # this mqtt client will publish
  mqtt_client_pub = paho.Client(client_id="inverter_controller_publisher" + random_string, transport="tcp", protocol=paho.MQTTv311,
                            clean_session=True)
  mqtt_client_pub.connected_flag=False
  mqtt_client_pub.on_publish = mqtt_on_publish
  mqtt_client_pub.on_connect = mqtt_on_connect
  mqtt_client_pub.username_pw_set(mqtt_username, mqtt_password)
  mqtt_client_pub.connect(mqtt_broker, mqtt_port, keepalive=60)
  mqtt_client_pub.loop_start()

  # this mqtt client will subscribe
  mqtt_client_sub = paho.Client(client_id="inverter_controller_subscriber" + random_string,transport="tcp",protocol=paho.MQTTv311,clean_session=True)  
  mqtt_client_sub.connected_flag=False
  mqtt_client_sub.on_subscribe = mqtt_on_subscribe
  mqtt_client_sub.on_connect = mqtt_on_connect
  mqtt_client_sub.on_message = mqtt_on_message
  mqtt_client_sub.username_pw_set(mqtt_username, mqtt_password)
  mqtt_client_sub.connect(mqtt_broker,mqtt_port,keepalive=60)
  mqtt_client_sub.loop_start()

  smarthome = False
  try:
    mqtt_smarthome_broker = config['mqtt_smarthome_broker']
    mqtt_smarthome_port = config['mqtt_smarthome_port']
    mqtt_smarthome_username = config['mqtt_smarthome_username']
    mqtt_smarthome_password = config['mqtt_smarthome_password']
    smarthome = True
  except KeyError:
    print_win("log","no Smarthome MQTT defined")

  mqtt_client_smarthome = ""

  if smarthome:
    mqtt_client_smarthome = paho.Client(client_id="inverter_monitor1" + random_string, transport="tcp",
                                        protocol=paho.MQTTv311,
                                        clean_session=True)
    mqtt_client_smarthome.on_publish = mqtt_on_publish
    mqtt_client_smarthome.on_connect = mqtt_on_connect
    mqtt_client_smarthome.username_pw_set(mqtt_smarthome_username, mqtt_smarthome_password)
    mqtt_client_smarthome.connect(mqtt_smarthome_broker, mqtt_smarthome_port, keepalive=60)
    mqtt_client_smarthome.loop_start()
  while not mqtt_client_sub.connected_flag or not mqtt_client_pub.connected_flag:
    print_win("log","Waiting for MQTT to connect\n")
    time.sleep(2)

def write_json(consumed,produced,power_now_grid,inverter_power):
  date_now = datetime.today().strftime('%Y%m%d')

  energy_data = { 'date_now':date_now, 'e_tot_produced': produced, 'e_tot_consumed': consumed, 'power_now_grid': power_now_grid, 'inverter_power': inverter_power} 
  if config['debug']: print_win("debug","Json:" + str(energy_data))
  f = open("/var/www/html/gridpoint.json", "w")
  f.write(json.dumps(energy_data))
  f.close()

def read_reg(client,rtype,register,count,id):
  rr = []
  ret = ""
  counter = 0
  response = False
  while counter < 4 and not response:
    try:
      if rtype == "holding":
        rr = client.read_holding_registers(register, count, id)
      if rtype == "input":
        rr = client.read_input_registers(register, count, id)
      response = True
    except Exception as e: 
      print_win("log",str(e))
      pass
    counter += 1
  try:
    if count == 1:
      ret = rr.registers[0]
    else:
      ret = rr.registers
  except:
    return ""
  return ret


def init_inverter(client,i):
  global inverter_data
  print_win("log","Getting initial data from:" + inverter_data[i]['name'] + "\n")
  p_batt_new = 200
  batt_mode = "charging"
  inverter_data[i]['inverter_registers'] = {}
  inverter_data[i]['p_batt'] = 0
  inverter_data[i]['PV power'] = 0
  inverter_data[i]['active_since'] = 0
  inverter_data[i]['p_last'] = 0
  inverter_data[i]['active'] = False

  if inverter_data[i]['type'] == "Gen24":
    try:
      v = read_reg(client,"holding",40351,1,1) / 100

      if v > 0 and v < 100:
        inverter_data[i]['Battery SoC'] = v
        print_win("log"," Got initial data from:" + inverter_data[i]['name'] + " - soc:" + str(v) + "\n")
        v = read_reg(client,"holding",40345,12,1)
        if v != "" and len(v) > 11: 
          inverter_data[i]['registers'] = v
      else:
        print_win("log"," Weird soc from:" + inverter_data[i]['name'])
        return False
    except:
      print_win("log"," No data from:" + inverter_data[i]['name'])
      pass
      return False
  if inverter_data[i]['type'] == "Sungrow":
    try:
      v = read_reg(client,"input",13022,1,1) / 10
      inverter_data[i]['Battery SoC'] = v 
      print_win("log"," Got initial data from:" + inverter_data[i]['name'] + " - soc:" + str(v) + "\n")
      v = read_reg(client,"holding",13049,3,1)
      if v != "" and len(v) > 1: 
        inverter_data[i]['registers'] = v
    except:
      print_win("log"," No data from:" + inverter_data[i]['name'] + "\n")
      pass
      return False
  return True

def clear_inverter(client,inverter_config):
  print_win("log","Clearing " + inverter_config['name'])
  p_batt_new = 0
  if inverter_data[i]['type'] == "Gen24":
    try:
        rr = rr_write(client,40348,0,0x01) # limit
        rr = rr_write(client,40355,0,0x01) # outwrte
        rr = rr_write(client,40356,0,0x01) # inwrte
    except:
      print_win("log"," error clearing " + inverter_config['name'] + "\n")
      pass
      return False
  if inverter_data[i]['type'] == "Sungrow":
    try:
      # EMS mode 0 = self consumption
      rr = rr_write(client,13049, 0, 0x01)
    except:
      print_win("log"," No data from:" + inverter_data[i]['name'] + "\n")
      pass
      return False
  return True


def set_power(client,inverter_data,power,mode,extra_mode):
  global config
  global external_vpp_stopped

  # Sungrow: set ems mode 0 => self-consumption
  # Gen24: set limit 0 => leaves as it is, need to change export limitation prio in gui

  if 'registers' in inverter_data:

    if external_vpp_stopped: # If VPP is stopped (or not running in vpp mode)
      if mode == "discharging":
        if int(inverter_data['Battery SoC']) < inverter_data['soc_low']:
          print_win("log", inverter_data['name'] + " - SoC is too low for discharging")
          power = 10
          mode = "charging"
      if mode == "charging":
        if 'max_power_pv' in inverter_data and 'PV power' in inverter_data:
          if power > int(inverter_data['max_power_pv']) and int(inverter_data['PV power']) > 1000:
            print_win("log"," Limiting power to " + str(inverter_data['max_power_pv']) + " due to inverter limitation when there is PV power - 20A limit???\n")
            power = 7200
        if int(inverter_data['Battery SoC']) > inverter_data['soc_high']:
          print_win("log", inverter_data['name'] + " - SoC is too high for charging\n")
          power = 10
          mode = "discharging"
    else: # VPP wants to charge/discharge, allow some extra percent of soc
      if mode == "discharging":
        if int(inverter_data['Battery SoC']) < inverter_data['soc_low_vpp']:
          print_win("log",inverter_data['name'] + " - SoC is too low for discharging, even in VPP mode\n")
          power = 10
          mode = "charging"
      if mode == "charging":
        if 'max_power_pv' in inverter_data and 'PV power' in inverter_data:
          if power > int(inverter_data['max_power_pv']) and int(inverter_data['PV power']) > 1000:
            print_win("log"," Limiting power to " + str(inverter_data['max_power_pv']) + " due to inverter limitation when there is PV power - 20A limit???\n")
            power = 7200
        if int(inverter_data['Battery SoC']) > inverter_data['soc_high_vpp']:
          print_win("log","", inverter_data['name'] + " - SoC is too high for charging, even in VPP mode\n")
          power = 10
          mode = "discharging"

    now = datetime.now()
    print_win("set",str(now) + " - Setting inverter " + inverter_data['name'] + " to mode: " + mode + " with " + str(power) + "W, SoC:" + str(float(inverter_data['Battery SoC'])) + "\n")

    if inverter_data['type'] == "Gen24":

      wchamax = inverter_data['registers'][0]
      limit = inverter_data['registers'][3]
      limit_new = 0
      outwrte = inverter_data['registers'][10]
      inwrte = inverter_data['registers'][11]
      if wchamax > 0:
        reg1 = 65535
        reg2 = 65535
        limit_new = 3

        if mode == "charging":
          reg2 = int(power * 10000 / wchamax)
          reg1 = int(65535 - (power * 10000 / wchamax)) + 2
          if reg1 > 65535:
            reg1 = 65535
        if mode == "discharging":
          reg1 = int(power * 10000 / wchamax)
          reg2 = int(65535 - (power * 10000 / wchamax)) + 2
          if reg2 > 65535:
            reg2 = 65535
        if extra_mode == "only_pv": #implies charging....
          reg1 = int(power * 10000 / wchamax)
          reg2 = 0
          if reg1 > 65535:
            reg1 = 65535
          limit_new = 3

        if abs(power) == 0:
          limit_new = 0
          reg1 = 0
          reg2 = 0

        print_win("log"," wchamax/outwrte/inwrte: " + str(wchamax) + "/" + str(reg1) + "/" + str(reg2) + "\n")

        same = False
        counter = 0
        while not same and counter < 2:

          try:
            if limit != limit_new:
              rr = rr_write(client,40348,limit_new,0x01) # limit
            if outwrte != reg1:
              rr = rr_write(client,40355,reg1,0x01) # outwrte
            if inwrte != reg2:
              rr = rr_write(client,40356,reg2,0x01) # inwrte
            v = read_reg(client,"holding",40345,12,1)
            read_reg1 = v[10]
            read_reg2 = v[11]
            read_limit = v[3]
          except:
            print_win("log"," error settting register " + inverter_data['name'])
            pass
            return False

          if read_reg1 == reg1 and read_reg2 == reg2 and read_limit == limit_new:
            same = True
          else:
            print_win("log","  not the same in registers, retrying\n")
            print_win("log","  new outwrte:" + str(reg1) + ", inwrte:" + str(reg2) + ", limit:" + str(limit_new) + "\n")
            print_win("log","  readoutwrte:" + str(read_reg1) + ", inwrte:" + str(read_reg2) + ", limit:" + str(read_limit) + "\n")
            limit = read_limit
            outwrte = read_reg1
            inwrte = read_reg2
            time.sleep(0.5)
            counter += 1

        print_win("log","  old outwrte:" + str(outwrte) + ", inwrte:" + str(inwrte) + ", limit:" + str(limit) + "\n")
        print_win("log","  new outwrte:" + str(reg1) + ", inwrte:" + str(reg2) + ", limit:" + str(limit_new) + "\n")
        print_win("log","  readoutwrte:" + str(read_reg1) + ", inwrte:" + str(read_reg2) + ", limit:" + str(read_limit) + "\n")

      else:
        print_win("log","No contact")

    if inverter_data['type'] == "Sungrow":
      reg = int(power)
      mode_int = 0
      if inverter_data['operating_mode'] == "forced":
        mode_int = 2
      if inverter_data['operating_mode'] == "virtual_power_plant":
        mode_int = 4

      if mode == "discharging":
        registers = [mode_int,0xbb,reg]
      if mode == "charging":
        registers = [mode_int,0xaa,reg]
      if power == 10 or mode == "stopped": # stop
        registers = [mode_int,0xcc,0]

      old_string = "Sungrow old:" + str(inverter_data['registers']) + ", new:" + str(registers)

      if inverter_data['registers'] != registers:
        if abs(inverter_data['registers'][2]-registers[2]) > 25: # Only updates if power differs more than 25 
          print_win("log"," - will update modbus registers")
          try:
            rr = rr_write(client,13049, registers, 0x01)   #aa bb cc charging/disch/stop
          except:
            print_win("log", old_string + ", error setting registers " + inverter_data['name'] + "\n")
            pass
            return False
        else:
          print_win("log", old_string + ", no need to update modbus registers since diff is so small\n")
      else:
        print_win("log", old_string + ", no need to update modbus registers\n")
    return
  return False


def connect_to_inverter(client, inverter_config):
  global inverter_data

  print_win("log","Connecting to:" + inverter_config['name'] + "\n")
  i = - 99
  for index,n in enumerate(inverter_data):
    if n['name'] == inverter_config['name']: i = index

  if i > -1:
    connected = False
    counter = 0
    while not connected and counter < 2:
      try:
        client.connect()
        counter += 1
        try:
          if inverter_config['type'] == "Gen24":
            rr = client.read_holding_registers(40345, 1, 0x01)
          if inverter_config['type'] == "Sungrow":
            rr = client.read_input_registers(13022, 1, 0x01)
          connected = True
        except:
          print_win("log"," Couldn't read tcp modbus from " + inverter_config['ip'])
          print_win("log"," Retrying to connect in 0.1s")
          time.sleep(0.1)
      except KeyboardInterrupt:
          exit(1)  
      except:
          print_win(" Retrying to connect in 1s")
          time.sleep(1)
    inverter_data[i]['connected'] = connected
    if connected: init_inverter(client,i)
  return connected

def rr_write(client,reg,value,unit_id):

  try:
    rr = client.write_registers(reg, value, unit_id)
  except Exception as e: 
    print_win("log",str(e))
    rr = False
    pass

  return rr

# Sungrow coding....
def get_value_from_32bits(r1, r2):
    v = r1 + (r2 & 32767) * 65536
    if r2 > 32768:
        v = - ((65535 - r1) + (65535 - r2) * 65536)
    return v

def decode_sungrow_system_state(register_value):
  state = ""
  if register_value & 64:
    state = "Running"
  if register_value & 16384:
    state = "Running in External EMS mode"
  return state

def decode_sungrow_running_state(register_value):

  coding = { '13000-input': { 'description': "Running State", 'type': "uint16", 'bit_decoder': {
      '0': "No power generated from PV|Power generated from PV",
      '1': "Not charging|Charging",
      '2': "Not discharging|Discharging",
      '3': "Load is reactive|Load is active",
      '4': "No power feed-in the grid|Power feed-in the grid",
      '5': "No power imported |Importing power from grid",
      '7': "No power generated from load|Power generated from load"
      }}
    }

  search_string = "13000-input"
  v = register_value
  new_v = ""
  for i in range(16):
      bit_value = 2**i
      bit_state = bit_value & int(v)
      if bit_state > 0:
          bit_state = 1
      search_v = f"{i}"
      if search_v in coding[search_string]['bit_decoder']:
          temp_vals = coding[search_string]['bit_decoder'][search_v].split("|")
          temp_v = temp_vals[bit_state]
          new_v += temp_v + "|"
  v = new_v

  return v


def clean_string(s: str) -> str:
    return "".join(filter(lambda x: x.isalnum() or x.isspace(), s))


def read_inverter_registers(client,config) -> int:
  global inverter_data
  global prev_pbatt
  global prev_mode

  # Read data from inverters...
  now_ms = round(time.time() * 1000)
  for i,n in enumerate(inverter_data):
    if inverter_data[i]['connected']:
      if inverter_data[i]['type'] == "Sungrow": 
        wrte = -1
        try:  # Sungrow is sloooow, so read as much as possible in larger chunks
          vi1 = read_reg(client[i],"input",4989,30,1)
          vi2 = read_reg(client[i],"input",5627,1,1)
          vi3 = read_reg(client[i],"input",12999,50,1)
          vh1 = read_reg(client[i],"holding",13049,32,1)
        except Exception as e: 
          print_win("log"," Couldn't read from tcp modbus for:" + inverter_data[i]['name'] + " " + str(e) + "\n")
          try:
            client[i].close()
          except Exception as e: 
            pass
          pass
        now_ms2 = round(time.time() * 1000)
        inverter_data[i]['inverter_registers']['4989-input'] = vi1[1:10]
        s = ""
        for idx in range (10):
            v0 = vi1[idx]
            v2 = v0 % 256
            v1 = v0 // 256
            s += chr(v1) + chr(v2)
        s = clean_string(s)
        inverter_data[i]['S/N'] = s
        v = vi1[18:]
        if v[0] > 32768:
          v[0] = v[0] - 65536
        inverter_data[i]['Inverter temperature'] = int(v[0]) / 10
        inverter_data[i]['inverter_registers']['5016-input'] = [ v[9],v[10] ]
        inverter_data[i]['PV power'] = v[9] + 65536 * v[10]
        inverter_data[i]['inverter_registers']['5627-input'] = [vi2]
        inverter_data[i]['Rated power'] = vi2 * 100
        inverter_data[i]['inverter_registers']['12999-input'] = [vi3[0]]
        inverter_data[i]['System state'] = vi3[0]
        inverter_data[i]['inverter_registers']['13000-input'] = [vi3[1]]
        inverter_data[i]['Running State'] = vi3[1]
        vtemp = vi3[10:12]
        inverter_data[i]['inverter_registers']['13009-input'] = vtemp
        v =  get_value_from_32bits(vtemp[0], vtemp[1])
        inverter_data[i]['Export Power'] = v
        inverter_data[i]['inverter_registers']['13021-input'] = [vi3[22]]
        inverter_data[i]['inverter_registers']['13022-input'] = [vi3[23]]
        inverter_data[i]['inverter_registers']['13023-input'] = [vi3[24]]
        inverter_data[i]['inverter_registers']['13024-input'] = [vi3[25]]
        p_batt = vi3[22] #always positive here :-(
        inverter_data[i]['Battery SoC'] = vi3[23] / 10
        inverter_data[i]['Battery SoH'] = vi3[24] / 10
        inverter_data[i]['Battery temperature'] = vi3[25]/ 10
        inverter_data[i]['inverter_registers']['13038-input'] = [vi3[39]]
        inverter_data[i]['Battery capacity'] = vi3[39] * 10  # Hmmm, on 24kWh...this needs to be *100, and on 82kWh *10...

        inverter_data[i]['registers'] = vh1[0:3]
        mode_byte = vh1[1]
        mode = "stopped"
    # '1': "Not charging|Charging"
    # '2': "Not Discharging|Discharging"
        if inverter_data[i]['Running State'] & 2:
          mode = "charging"
        if inverter_data[i]['Running State'] & 4:
          mode = "discharging"
        print_win("log",mode)
        # if mode_byte == 170: mode = "charging"
        # if mode_byte == 187: mode = "discharging"
        # if mode_byte == 204: mode = "stopped"
        if prev_mode == 0:
          prev_mode = mode_byte
        inverter_data[i]['inverter_registers']['13049-holding'] = [vh1[0]]
        inverter_data[i]['inverter_registers']['13050-holding'] = [vh1[1]]
        inverter_data[i]['inverter_registers']['13051-holding'] = [vh1[2]]
        inverter_data[i]['mode'] = mode
        inverter_data[i]['mode_byte'] = mode_byte
        inverter_data[i]['EMS mode'] = vh1[0]
        inverter_data[i]['Battery forced charge discharge cmd'] = vh1[1]
        inverter_data[i]['Battery forced charge discharge power'] = vh1[2]
        wrte = vh1[2]
        inverter_data[i]['p_batt_reg'] = wrte
        inverter_data[i]['p_inverter'] = 0
        if mode == "stopped" and p_batt > 300: # Assume that below this, only noise. Otherwise, some leftovers, keep reporting and keep the sign from last value when stopped
          p_batt = prev_pbatt
          if prev_mode == "discharging": # Switch sign if we were discharging before
            p_batt = -p_batt
        else: # Charging or discharging...
          if mode == "discharging":
            p_batt = -p_batt
        inverter_data[i]['Battery power'] = p_batt
        inverter_data[i]['p_inverter'] = inverter_data[i]['PV power'] - inverter_data[i]['Battery power']
        now = datetime.now()
        current_time = now.strftime("%H:%M:%S")

        print_win("log",str(current_time) + " p_batt:" + str(inverter_data[i]['Battery power']) + ", prev p_batt:" + str(prev_pbatt) + ", prev mode:" + str(prev_mode) + ", ems mode:" + str(inverter_data[i]['mode_byte']) + "\n")
        prev_pbatt = copy.deepcopy(p_batt)
        if mode != prev_mode:
          print_win("log","CHANGING MODE:" + str(prev_mode) + ">" + str(mode) + "\n")
          prev_mode = copy.deepcopy(mode)
        inverter_data[i]['EMS Hearbeat'] = vh1[30]
        inverter_data[i]['inverter_registers']['13079-holding'] = vh1[30]

      if inverter_data[i]['type'] == "Gen24":
        outwrte = -1
        inwrte = -1
        wchamax = -1
        v = read_reg(client[i],"holding",40102,1,1)
        if v > 32768:
          v = v -65536
        inverter_data[i]['temperature'] = int(v) / 10
        v = read_reg(client[i],"holding",40345,12,1)
        if v != "" and len(v) > 11: 
          inverter_data[i]['registers'] = v
          wchamax = v[0]
          inverter_data[i]['Battery SoC'] = v[6] / 100
          charging_state = v[9]
          outwrte = int(v[10]) # Discharge
          inwrte = int(v[11]) # Charge
          p_batt = 0
          v = read_reg(client[i],"holding",40083,2,1) # scale_factor power
          if v != "":
            scale_down = 10**(65536 - v[1]) 
            if v[0] > 32768:
              v[0] = 65536 - v[0]
            p_ac = v[0] / scale_down
            v = read_reg(client[i],"holding",40257,1,1) # scale_factor power
            if v != "":
              scale_down = 10**(65536 - v) 
              scale_factor = v
              v = read_reg(client[i],"holding",40274,70,1) # MPTT registers
              if v != "" and len(v) > 1: 
                pv1 = v[0] / scale_down
                pv2 = v[20] / scale_down
                p_batt = v[60] / scale_down
                pv = pv1 + pv2
                pv = format_float(2,pv)
                inverter_data[i]['PV power'] = pv
                inverter_data[i]['p_ac'] = p_ac
                if p_batt == 0: # During charge, p_batt is 0...compute it instead
                  p_batt = pv - p_ac
                if charging_state == 3:
                  p_batt = -abs(p_batt)
                  inverter_data[i]['mode'] = "discharging"
                else:
                  p_batt = abs(p_batt)
                  inverter_data[i]['mode'] = "charging"
                inverter_data[i]['Battery power'] = p_batt
                if wchamax > 0: # when battery is misbehaving, wchamax = 0
                  inverter_data[i]['p_inverter'] = pv - inverter_data[i]['Battery power']
                  inverter_data[i]['p_inverter'] = format_float(2,inverter_data[i]['p_inverter'])
                  if outwrte < 32768:
                    inverter_data[i]['mode'] = "discharging"

  now_ms2 = round(time.time() * 1000)
  return now_ms2-now_ms


def adjust_power(p,client,config,grid_ignore):
  global inverter_data
  global inverter_monitor_data
  global mode_last
  global mutex
  global p_grid


  mutex = 1
  # positive value means we are consuming from grid
  print_win("adjust","Will adjust overall P to:" + str(p) + ", ignoring grid:" + str(grid_ignore)) # p<0 discharging

  # Check the status of the inverters...

  num_active_inverters = 0
  now = datetime.now()
  print_win("log","---- " + str(now) + " ----\n")
  
  for i,n in enumerate(inverter_data):

    # inverter data comes from mqtt
    if config['debug']: print_win("debug"," Inv:" + str(inverter_data[i]) + "\n")

    if inverter_data[i]['connected']:
      if inverter_data[i]['type'] == "Sungrow":
        num_active_inverters += 1

      if inverter_data[i]['type'] == "Gen24":
        num_active_inverters += 1

  if config['follow_inverter'][0]['enabled'] == True:
    if 'name' in inverter_monitor_data:
      if config['follow_inverter'][0]['name'] == inverter_monitor_data['name']:
        if inverter_monitor_data['mode'] == 'discharging' and inverter_monitor_data['Battery forced charge discharge power'] >= config['follow_inverter'][0]['discharge_power_trigger']:
          grid_ignore = 1 # WIP
          p = config['follow_inverter'][0]['discharge_power_percentage']/100
          print_win("log"," FOLLOWING DISCHARGE, P:" + str(inverter_monitor_data['Battery forced charge discharge power']) + "W\n")
        elif inverter_monitor_data['mode'] == 'charging' and inverter_monitor_data['Battery forced charge discharge power'] >= config['follow_inverter'][0]['charge_power_trigger']:
          # If charging, just let it charge and adjust to grid or schedule
          print_win("log"," FOLLOWING CHARGE, P:" + str(inverter_monitor_data['Battery forced charge discharge power'] + "W\n"))

        else:
          print_win("log"," NOT FOLLOWING, TOO LOW P:" + str(inverter_monitor_data['Battery forced charge discharge power'] + "W\n"))

  if grid_ignore:
    # Set power on all inverters...
    i = 0
    while i < len(inverter_data):
      if inverter_data[i]['connected']:
        if p > 1:
          p = 1
        power = p * inverter_data[i]['max_power']
        if power > 0:
          mode = "discharging"
        else:
          mode = "charging"
        print_win("log","Ignoring grid and setting power to:" + str(power) + " for inverter " + inverter_data[i]['name'] + "\n")
        set_power(client[i],inverter_data[i],abs(power),mode,"")
      i += 1
    return

  # sort high score lists

  energy_high_score_list = []
  energy_high_score_list_index = []

  soc_high_score_list = []
  soc_high_score_list_index = []

  pv_high_score_list = []
  pv_high_score_list_index = []


  for index,j in enumerate(inverter_data):
    e_left = format_float(2,(j['Battery SoC'] * j['full_battery_kwh'] * 0.01))
    j['e_left'] = e_left
    energy_high_score_list.append(e_left)
    energy_high_score_list_index.append(index)
    soc_high_score_list.append(j['Battery SoC'])
    soc_high_score_list_index.append(index)
    pv_high_score_list.append(j['PV power'])
    pv_high_score_list_index.append(index)

  i = 0
  while i < len(inverter_data) - 1:
    j = 0
    while j < len(inverter_data) - 1:
      if pv_high_score_list[j] < pv_high_score_list[j+1]:
        tmp = pv_high_score_list[j+1]
        tmp_index = pv_high_score_list_index[j+1]
        pv_high_score_list[j+1] = pv_high_score_list[j]
        pv_high_score_list_index[j+1] = pv_high_score_list_index[j]
        pv_high_score_list[j] = tmp
        pv_high_score_list_index[j] = tmp_index
      if soc_high_score_list[j] < soc_high_score_list[j+1]:
        tmp = soc_high_score_list[j+1]
        tmp_index = soc_high_score_list_index[j+1]
        soc_high_score_list[j+1] = soc_high_score_list[j]
        soc_high_score_list_index[j+1] = soc_high_score_list_index[j]
        soc_high_score_list[j] = tmp
        soc_high_score_list_index[j] = tmp_index
      if energy_high_score_list[j] < energy_high_score_list[j+1]:
        tmp = energy_high_score_list[j+1]
        tmp_index = energy_high_score_list_index[j+1]
        energy_high_score_list[j+1] = energy_high_score_list[j]
        energy_high_score_list_index[j+1] = energy_high_score_list_index[j]
        energy_high_score_list[j] = tmp
        energy_high_score_list_index[j] = tmp_index
      j += 1
    i += 1


  # Now, go through the list and disable the ones that need disabling due to soc
  i = 0
  while i < len(inverter_data):
    index = energy_high_score_list_index[i]
    if config['debug']: print_win("debug"," check soc disable - " + inverter_data[index]['name'] + " - soc:" + str(inverter_data[index]['Battery SoC']) + ", soc low:" + 
      str(inverter_data[index]['soc_low']) + ", soc high:" + str(inverter_data[index]['soc_high']))
    if inverter_data[index]['Battery SoC'] < inverter_data[index]['soc_low'] + config['soc_hysteresis']:
      inverter_data[index]['active'] = False 
      inverter_data[i]['active_since'] = 0
      if config['debug']: print_win("debug","  SoC low")
    i += 1

  now = datetime.now(pytz.timezone('Europe/Stockholm'))
  ts = int(now.strftime("%S")) + 60 * int(now.strftime("%M")) + 3600 * int(now.strftime("%H"))

  # Go through and see if there is still an inverter active, then keep this one...unless it has been active a long time

  # TODO: make a list of last Pgrid...if it's always over x, try to switch inverter (sort max power)

  i = 0
  active_inverter_index = -1
  while i < len(inverter_data):
    index = energy_high_score_list_index[i]
    if config['debug']: print_win("debug"," check last active - " + inverter_data[index]['name'] + str(inverter_data[index]['active']) + str(inverter_data[index]['active_since']))
    if inverter_data[index]['active']:
      if inverter_data[index]['connected']:
        ts_start = inverter_data[index]['active_since']
        if ts > ts_start + config['active_time']: # try to change active one
          inverter_data[index]['active'] = False 
        else:
          active_inverter_index = index
          i = len(inverter_data)
      else:
        if config['debug']: print_win("debug","  I was active, but now I'm offline")
        inverter_data[index]['active'] = False 
    i += 1


  # Now, go through the list and choose the one with highest soc / energy left and soc is ok
  # Others will be allowed to charge from pv
  
  if active_inverter_index < 0:
    i = 0
    active_inverter_index = -1
    if config['active_inverter_choice'] == "e_remaining":
      while i < len(inverter_data):
        index = energy_high_score_list_index[i]
        if config['debug']: print_win("debug"," check highscore - " + inverter_data[index]['name'] + str(energy_high_score_list[i]) + " kWh left")
        if inverter_data[index]['Battery SoC'] > inverter_data[index]['soc_low'] + config['soc_hysteresis'] and active_inverter_index < 0:
          if inverter_data[index]['connected']:
            if config['debug']: print_win("debug","  I'm the winner!")
            if not inverter_data[index]['active']:
              inverter_data[index]['active_since'] = ts
              inverter_data[index]['active'] = True
            active_inverter_index = index
            i = len(inverter_data)
        i += 1
    if config['active_inverter_choice'] == "highest_soc":
      while i < len(inverter_data):
        index = soc_high_score_list_index[i]
        if config['debug']: print_win("debug"," check highscore - " + str(inverter_data[index]['name']) + str(soc_high_score_list[i]) + " %")
        if inverter_data[index]['Battery SoC'] > inverter_data[index]['soc_low'] + config['soc_hysteresis'] and active_inverter_index < 0:
          if inverter_data[index]['connected']:
            if config['debug']: print_win("debug","  I'm the winner!")
            if not inverter_data[index]['active']:
              inverter_data[index]['active_since'] = ts
              inverter_data[index]['active'] = True
            active_inverter_index = index
            i = len(inverter_data)
        i += 1


  if active_inverter_index > -1:
    print_win("log"," Inverter:" + inverter_data[active_inverter_index]['name'] + "will be active")
  else:     # Choose the one with highest pv
    print_win("log"," Bad SoC on all inverters, checking highest pv")
    while i < len(inverter_data):
      index = pv_high_score_list_index[i]
      if config['debug']: print_win("debug"," check pv highscore - " + inverter_data[index]['name'] + str(pv_high_score_list[i]) + " %")
      if active_inverter_index < 0:
        if inverter_data[index]['connected']:
          print_win("log","  I'm the winner!")
          if config['debug']: print_win("debug","  I'm the winner!")
          if not inverter_data[index]['active']:
            inverter_data[index]['active_since'] = ts
            inverter_data[index]['active'] = True
          active_inverter_index = index
          i = len(inverter_data)
      i += 1

  # Set power on all inverters...
  i = 0
  active_inverter_index = -1
  while i < len(inverter_data):
    index = energy_high_score_list_index[i]
    if inverter_data[index]['active']:
      p_diff = p
      if inverter_data[index]['connected']:
        if 'mode' in inverter_data[index]:
          if inverter_data[index]['operating_mode'] == 'self_optimize':
            ret = set_self_optimize(client,config,index)  
          if inverter_data[index]['operating_mode'] == 'forced':
            ret = adjust_inverter_power(p_diff,client,config,index,grid_ignore)
          if inverter_data[index]['operating_mode'] == 'virtual_power_plant':
            ret = adjust_inverter_power(p_diff,client,config,index,grid_ignore)

    else:  
      p_diff = p
      if inverter_data[index]['connected']:
        if inverter_data[index]['PV power'] < 50:
          if inverter_data[index]['operating_mode'] == 'virtual_power_plant' or inverter_data[index]['operating_mode'] == 'forced':
            set_power(client[index],inverter_data[index],10,"charging","")
          else:
            ret = set_self_optimize(client,config,index)  
        else:
          if inverter_data[index]['operating_mode'] == 'virtual_power_plant' or inverter_data[index]['operating_mode'] == 'forced':
            set_power(client[index],inverter_data[index],inverter_data[index]['PV power'],"charging","")
          else:
            ret = set_self_optimize(client,config,index)  
    i += 1

  return



def format_float(decimals,f):

  format_string = "{:.2f}"
  format_string = "{:." + str(decimals) + "f}"

  return float(format_string.format(f))


def set_self_optimize(client,config,index):
  global inverter_data

  # Negative values means consuming from grid
  print_win("log"," Will set " + str(inverter_data[index]['name']) + " in self optimization mode")

  if inverter_data[index]['type'] == "Sungrow":
    inverter_mode = inverter_data[index]['registers'][0]
    if inverter_mode != 1:
      registers = [0,0xcc,0]
      try:
        rr = rr_write(client[index],13049, registers, 0x01)   #aa bb cc charging/disch/stop
      except:
        print_win("log"," error setting registers " + inverter_data['name'])
        pass
        return False
    else:
      print_win("log","  Already set")
      
  if inverter_data[index]['type'] == "Gen24":
    inverter_mode = inverter_data[index]['registers'][3]
    if inverter_mode != 0:
      limit_new = 0
      try:
        rr = rr_write(client[index],40348,limit_new,0x01) # limit
      except:
        print_win("log"," error setting registers " + inverter_data['name'])
        pass
        return False
    else:
      print_win("log","  Already set")

  return True



def adjust_inverter_power(p_diff_inverter,client,config,index,grid_ignore):
  global inverter_data

  # Negative values means consuming from grid
  print_win("log"," Will adjust:" + str(inverter_data[index]['name']) + "p_diff: " + str(p_diff_inverter))

  mode = "charging"
  p_new = 0
  mode = inverter_data[index]['mode']
  if mode == "stopped":
    mode = "charging"

  if inverter_data[index]['type'] == "Sungrow":
    p_conf = inverter_data[index]['Battery forced charge discharge power']
    if mode == "discharging":
      mode_new = mode
      p_conf = -p_conf
      p_new = p_conf - p_diff_inverter
      if config['debug']: print_win("debug","   p_conf" + str(p_conf) + "p_diff" + str(p_diff_inverter) + "p_new" + str(p_new))
      if p_conf - p_diff_inverter > 0: # need to change mode to charging a bit
        mode_new = "charging"
        p_new = abs(p_conf - p_diff_inverter)
    else:
      mode_new = mode
      p_new = p_conf - p_diff_inverter
      if config['debug']: print_win("debug","   p_conf" + str(p_conf) + "p_diff" + str(p_diff_inverter) + "p_new" + str(p_new))
      if p_conf - p_diff_inverter < 0: # need to change mode to discharging a bit
        mode_new = "discharging"
        p_new = abs(p_conf - p_diff_inverter)
    p_new = abs(p_new)

  if inverter_data[index]['type'] == "Gen24":
    print_win("log","  REG: " + inverter_data[index]['registers'])
    wchamax = inverter_data[index]['registers'][0]
    outwrte = inverter_data[index]['registers'][10] 
    inwrte = inverter_data[index]['registers'][11] 
    if mode == "discharging":
      mode_new = mode
      p_conf = outwrte * wchamax / 10000
      if p_conf > 32768:
        p_conf = p_conf - 65535
      if p_conf == 0:
        p_conf = inverter_data[index]['p_last']
      p_new = -p_conf - p_diff_inverter
      print_win("log","   disch now: p_conf" + str(p_conf) + "p_diff" + str(p_diff_inverter) + "p_new" + str(p_new))
      if p_new > 0: # need to change mode to charging a bit
        mode_new = "charging"
        p_new = abs(-p_conf - p_diff_inverter)
    else:
      mode_new = mode
      p_conf = inwrte * wchamax / 10000
      if p_conf == 0:
        p_conf = inverter_data[index]['p_last']
      p_new = p_conf - p_diff_inverter
      print_win("log","  ch now -p_conf" + str(p_conf) + "p_diff" + str(p_diff_inverter) + "p_new" + str(p_new))
      if p_conf - p_diff_inverter < 0: # need to change mode to discharging a bit
        mode_new = "discharging"
        p_new = abs(p_conf - p_diff_inverter)
    p_new = abs(p_new)

  if config['debug']: print_win("debug","  current mode:" + str(mode) + " configured Pbatt:" + str(p_conf))
  if config['debug']: print_win("debug","  new mode:" + str(mode_new) + " power new:" + str(p_new))

  if p_new > inverter_data[index]['max_power']:
    p_new = inverter_data[index]['max_power']

  set_power(client[index],inverter_data[index],p_new,mode_new,"")
  inverter_data[index]['p_last'] = p_new
  return True

def check_if_time_to_publish(config,counter):
  global mqtt_client_pub
  global mqtt_client_smarthome
  global smarthome
  global inverter_data
  global inverter_data_old

  if config['debug']: print_win("debug","Check publish time???")
  for i2,i_data in enumerate(inverter_data):
    if 'registers' in i_data:
      json_string = json.dumps(i_data, separators=(',', ':'))
      force_update = False
      if len(inverter_data_old) == 0 or counter % 20 != 1:
        force_update = True
      if not force_update:
        if i_data == inverter_data_old[i2]:
          if config['debug']: print_win("debug","Inverter data has not changed")
        else:
          force_update = True
      if force_update:
        json_string = json.dumps(i_data, separators=(',', ':'))
        #if config['debug']: print_win("debug","New data: " + json_string)
        rc = publish(mqtt_client_pub, i_data, "inverter/monitor")
        if smarthome:
          rc = publish(mqtt_client_smarthome, i_data, "inverter/monitor/" + i_data['name'])
        while len(inverter_data_old) < len(inverter_data):
          inverter_data_old.append({})
        inverter_data_old[i2].update(i_data)

def mqtt_on_publish(mqtt_client, userdata, result):
  # print_win("log","data published:" + result)
  pass


def publish(mqtt_client, pub_var, topic):
  global config

  json_string = json.dumps(pub_var)
  ret = mqtt_client.publish(topic, json_string)
  if config['debug']: print_win("debug","Publishing in:" + topic + "\n") # + ":" + json_string)


async def send_keepalives(inverter_data,config,client):
  global prev_pbatt
  global prev_mode

  interval_register_vpp_keepalive = 20  # keepalive value in inverter register

  mismatching_state_counter = 0  # counts if mode and running state differs
  while True:
    if not config['only_monitor']:
      for i2,i_data in enumerate(inverter_data):
        if i_data['operating_mode'] == 'virtual_power_plant':
          try:
            if config['inverters'][i2]['type'] == "Sungrow":
              rr = rr_write(client[i2],13079, interval_register_vpp_keepalive, 0x01)
              print_win("vpp","Sent keepalive to:" + config['inverters'][i2]['name'] + ": " + str(interval_register_vpp_keepalive) + "s\n")
              # Also check running state/system state to see if that seems ok
              running_state = decode_sungrow_running_state(i_data['Running State'])
              system_state = decode_sungrow_system_state(i_data['System state'])
              print_win("log","SYSTEM:" + str(system_state) + ", RUN:" + str(running_state) + "mode:" + str(i_data['Battery forced charge discharge cmd']))
              print_win("log","Batt power reg:" + str(i_data['Battery forced charge discharge power']) + ", real:" + str(i_data['Battery power']) + ", counter:" + 
                str(mismatching_state_counter) + ", EMS hb:" + str(i_data['EMS Hearbeat']) + "\n")
              if i_data['Battery forced charge discharge cmd'] == 170 and i_data['Battery forced charge discharge power'] > 300: # charge
                if '|Charging|' in running_state and '|Not discharging|' in running_state:
                  mismatching_state_counter = 0
                else:
                  mismatching_state_counter += 1
              if i_data['Battery forced charge discharge cmd'] == 187 and i_data['Battery forced charge discharge power'] > 300: # discharge
                if '|Not charging|' in running_state and '|Discharging|' in running_state:
                  mismatching_state_counter = 0
                else:
                  mismatching_state_counter += 1
              if i_data['Battery forced charge discharge cmd'] == 204: # stop
                if '|Not charging|' in running_state and '|Not discharging|' in running_state:
                  mismatching_state_counter = 0
                else:
                  mismatching_state_counter += 1
              if mismatching_state_counter > 5:
                syslog.syslog(syslog.LOG_INFO, "ERROR: mismatching register and mode for some time, restart? SoC:" + str(i_data['Battery SoC']) + running_state + " " + str(i_data['Battery forced charge discharge cmd']))
          except:
            print_win("log"," Couldn't send tcp modbus to " + i_data['ip'] + "\n")       
    await asyncio.sleep(2)
        

async def read_from_inverter(inverter_data,config,client):

  interval = 1
  counter = 0
  while True:
    # Read all registers
    now_ms = round(time.time() * 1000)
    # Check if all inverters are still connected
    for i2,i_data in enumerate(inverter_data):
      if not i_data['connected']:
        connect_to_inverter(client[i2],config['inverters'][i2]) 
    if config['debug']: print_win("debug","Updating registers")
    time_took = read_inverter_registers(client,config)
    check_if_time_to_publish(config,counter)
    counter +=1
    if counter > 1000:
      counter = 0    
    sleep_time = format_float(3,interval - time_took/1000)
    if sleep_time > 5:
      sleep_time = interval
    if sleep_time > 0:
      if config['debug']: print_win("debug","Sleeping from inverter read:" + str(sleep_time))
      await asyncio.sleep(sleep_time)


async def start_tasks_if_needed(inverter_data,config,client):

  tasks = str(asyncio.all_tasks())
  if 'read_from_inverter' not in tasks:
    print_win("log","Started task for reading from inverter\n")
    asyncio.create_task(read_from_inverter(inverter_data,config,client))
    syslog.syslog(syslog.LOG_INFO, "Started task:read_from_inverter\n")
  if 'send_keepalives' not in tasks:
    print_win("log","Started task for sending EMS keepalives\n")
    asyncio.create_task(send_keepalives(inverter_data,config,client))
    syslog.syslog(syslog.LOG_INFO, "Started task:send_keepalives\n")


async def run_server():
  global inverter_data
  global inverter_data_old
  global config
  global mutex
  global inverter_monitor_data
  global adjust_now
  global p_grid
  global last_updated
  global external_vpp_stopped
  global vpp_control
  global client
  global external_vpp_passthrough
  global prev_pbatt
  global prev_mode

  vpp_control = {}
  client = []
  inverter_data = []
  inverter_data_old = []
  inverter_monitor_data = {}
  last_updated = ""
  e_consumed = 0
  e_produced = 0
  wchamax = 0
  mode_last = "idle"
  p_grid = 0
  prev_pbatt = 0
  prev_mode = 0
  mutex = 0
  adjust_now = False
  external_vpp_stopped = True

  interval_change_power = int(10 * config['interval_change_power'])            # x 100ms
  if config['debug']: print_win("debug",str(config['inverters']))
  external_vpp_passthrough = config['external_vpp_passthrough']

  for i,inverter in enumerate(config['inverters']):
    if config['debug']: print_win("debug",str(i) + inverter['name'])
    client.append( ModbusClient(inverter['ip'], port=502) )
    inverter_data.append(inverter)
    connect_to_inverter(client[-1],inverter)
    # inverter_data will contain inverter_config, with same index as client

  if len(sys.argv) > 1:
    if sys.argv[1] == "clear":
      for i,inverter in enumerate(config['inverters']):
        clear_inverter(client[i],inverter)

      print_win("log","cleared registers in all inverters\n")
      exit(1)

  run_mqtt_client(config)

  all_mqtt_data_is_there = False

  rc = await start_tasks_if_needed(inverter_data,config,client)

  interval_counter = 0
  while not all_mqtt_data_is_there:
    interval_counter += 1
    print_win("log","Waiting for mqtt data\n")
    await asyncio.sleep(0.5)
    all_ok = True
    for i,i_data in enumerate(inverter_data):
      if not 'mode' in i_data or not 'registers' in i_data:
        all_ok = False
        if interval_counter > 20:
          exit(1)
    if all_ok:
      all_mqtt_data_is_there = True

  interval_counter = 0
  while True:
    interval_counter += 1
    if interval_counter % interval_change_power == 2:
      rc = await start_tasks_if_needed(inverter_data,config,client)

    if interval_counter % 100 == 5: # Curses not working that well, writes in bad positions sometimes
      if config['use_curses']:
        windows_update()

    p = p_grid
    print_win("grid","Current P:" + str(p))
    # Only adjust power if we are not curently controlled by an external ems, or if we only monitor the inverter(s)
    if (not config['external_vpp_overrides'] or external_vpp_stopped) and (not external_vpp_passthrough):
      # Check if it's time to change power
      if not config['only_monitor'] and ((interval_counter % interval_change_power == 1) or adjust_now):
        if adjust_now:
          print_win("log","Got MQTT trigger, running with less sleep...")
        adjust_now = False
        p = p_grid
        now = datetime.now(pytz.timezone('Europe/Stockholm'))
        ts = int(now.strftime("%S")) + 60 * int(now.strftime("%M")) + 3600 * int(now.strftime("%H"))
        if config['debug']: print_win("debug","Time: " + str(ts)) 
        grid_ignore = False
        if config['time_control']:
          for time_control_item in config['time_control']:
            start = time_control_item['start_time']
            end = time_control_item['end_time']
            start_time = int(start[0:2]) * 3600 + int(start[3:5]) * 60 + int(start[6:8])
            end_time = int(end[0:2]) * 3600 + int(end[3:5]) * 60 + int(end[6:8])
            if start_time > end_time: # passes midnight
              if ts > 0 and ts < start_time : # after midnight?
                start_time = 0
              elif ts < 24*3600 and ts > start_time: # before midnight
                end_time = 24*3600 - 1
            if start_time < ts and end_time > ts:
              p = time_control_item['power']
              if config['debug']: print_win("debug","Schedule says it's time for static charge/discharge:" + str(p))
              grid_ignore = True  
        while mutex == 1:
          await asyncio.sleep(0.1)
        p_inv = adjust_power(p,client,config,grid_ignore)
        mutex = 0
        #write_json(e_consumed,e_produced,p,p_inv)

    if interval_counter == (interval_change_power):
      interval_counter = 0
    await asyncio.sleep(0.1)


def print_win(window_name,text):
  global window,win_grid,win_log,win_adjust,win_vpp,win_set
  global config

  if config['use_curses']:
    if window_name == "grid":
      rows, cols = win_grid.getmaxyx()
      win_grid.addstr(1,1,text + " " * (cols-len(text) - 3))
      win_grid.refresh()

    if window_name == "adjust":
      rows, cols = win_adjust.getmaxyx()
      win_adjust.addstr(1,1,text + " " * (cols-len(text) - 3))
      win_adjust.refresh()

    if window_name == "set":
      rows, cols = win_set.getmaxyx()
      y, x = win_set.getyx()
      win_set.addstr(y,1,text + " " * (cols-len(text) - 3))
      win_set.refresh()

    if window_name == "log":
      rows, cols = win_log.getmaxyx()
      y, x = win_log.getyx()
      win_log.addstr(y,1,text + " " * (cols-len(text) - 3))
      win_log.refresh()

    if window_name == "vpp":
      rows, cols = win_vpp.getmaxyx()
      y, x = win_vpp.getyx()
      win_vpp.addstr(y,1 ,text + " " * (cols-len(text) - 3))
      win_vpp.refresh()
  else:
    if re.search(".*\n$",text):
      print(text,end='')
    else:
      print(text)


def windows_update():
  global window,win_grid,win_log,win_adjust,win_vpp,win_set

  curses.resizeterm(*window.getmaxyx())
  window.refresh()
  print_win("log","Updated curses\n")

def start_windows(stdscr) -> int:
  global window,win_grid,win_log,win_adjust,win_vpp,win_set

  stdscr = curses.initscr()
  stdscr.clear()
  window = stdscr
  rows, cols = stdscr.getmaxyx()
  if rows < 30 or cols < 100:
    return rows, cols
  curses.curs_set(0)
  window.addstr(3,0,"─── Inverter " + "─" * (cols - 15))
  window.addstr(13,0,"─── VPP " + "─" *  (cols - 10))
  window.addstr(24,0,"─── Log " + "─" *  (cols - 10))
  window.refresh()
  win_grid = stdscr.subwin(3,25,0,0)
  win_adjust = stdscr.subwin(3,60,0,27)
  win_set = stdscr.subwin(9,cols - 60,4,0)
  win_vpp = stdscr.subwin(10,cols,14,0)
  win_log = stdscr.subwin(rows-25,cols,25,0)
  win_grid.box()
  win_adjust.box()
  win_log.scrollok(True)
  win_set.scrollok(True)
  win_vpp.scrollok(True)
  windows_update()
  asyncio.run(run_server())
  return 0

def startmeup():
  global config

  syslog.syslog(syslog.LOG_INFO, "Starting inverter controller")
  print("\nStarting inverter controller\nReading config from file\n")
  inverter_controller = InverterController()
  config = inverter_controller.config
  if config['use_curses']:
    rows, cols = wrapper(start_windows)
    if rows > 0:
      print(rows,"x",cols)
      print("Too small window to run this in curses!")
    exit(1)

  asyncio.run(run_server())

if __name__ == "__main__":
  startmeup()
