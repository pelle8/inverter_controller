# inverter_controller

OK, so you have a couple of inverters and batteries and want to use them all in a self optimizing mode?

Here is what you need...
- A smartmeter 
- An RPi that can talk modbus with the inverters, and receive grid info from a smartmeter (over mqtt)


## How does it work?

A script (on an RPi) queries a smartmeter connected at the gridpoint, and publishes the result over mqtt. (Look in smartmeter repo). Another script monitors inverter registers (soc, power etc) and publishes this over mqtt.
A third script gets power grid and inverter info from mqtt subscriptions and interacts with the configured inverters with the aim to have 0W at grid connection point.
If inverters have a soc below a certain level, discharging will be disabled for these. While others will take the load. Same goes for high soc.
A schedule for forced charge/discharge and optimizations can be configured.


## My setup

- One RPi with RS485 modbus connected to Sungrow DTSU666 smartmeter, publishes over mqtt.
- On the RPi above, the "inverter_controller"-script runs, which includes both monitoring and controlling.
- One RPi with one can-interface to Tesla M3 BMS and another can-interface to Sungrow SH10RT.
- One RPi with one can-interface to Leaf BMS and one RS485 modbus to Fronius Gen24 (6kW). This has now changed, I now have a Sungrow SH10RT instead of the Gen24.


## Getting started

- Adjust inverter_controller.yaml to match your setups.
- Make sure the smartmeter mqtt is runnning (check out smartmeter repo)
- Start "inverter_controller.py"

## Known limitations/TODO

- Currently only supports: Fronius Gen24 and Sungrow SH10RT
